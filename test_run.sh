#!/usr/bin/env bash

# activate environment
source ./venv/bin/activate

./VGA.py -t 32 -c 0.0 ./input/genomes/mulford.fsa ./input/databases/poxviridae_all_genomes_from_ncbi.gb &&
./VGA.py -t 32 -c 0.7 ./input/genomes/mulford.fsa ./input/databases/poxviridae_all_genomes_from_ncbi.gb &&
./VGA.py -t 32 -c 0.0 -p KX061501 -p M35027 ./input/genomes/mulford.fsa ./input/databases/poxviridae_all_genomes_from_ncbi.gb &&
./VGA.py -t 32 -c 0.7 -p KX061501 -p M35027 ./input/genomes/mulford.fsa ./input/databases/poxviridae_all_genomes_from_ncbi.gb

