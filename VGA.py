#!/usr/bin/env python3

# 2017 Mathias Wajnberg
# 2020 Andreas Andrusch

import argparse
import os
import subprocess
import sys
from copy import deepcopy as deepcopy

from Bio import SeqIO
from Bio.Alphabet import IUPAC
from Bio.Blast import NCBIXML
from Bio.Blast.Applications import NcbiblastxCommandline
from Bio.Seq import Seq


def setup_cli_parser():
    parser = argparse.ArgumentParser(
        prog='Viral Genome Annotator',
        description='This program annotates a given fasta file with a DNA sequence of a virus genome using blastp '
                    'against a given reference database in genbank file format. As a first step, ORFs are searched in '
                    'the genome which are then translated into their corresponding amino acid sequences. These are '
                    'searched for in the blastp database thas is built from the amino acid sequences extracted from '
                    'the given genbank database. The output is a newly created genbank file which includes the input '
                    'sequence, all annotated ORFs in one feature track as well as all unannotated ORFs in another '
                    'feature track. Since this tool makes use of the NCBI blast+ programs, they must be in the PATH.')
    parser.add_argument('input', help='Sets the path to the input fasta file with the virus genome.')
    parser.add_argument('database', help='Sets the path to the database genbank file.')
    parser.add_argument('-m', '--minimum-length', type=int, default=93,
                        help='Sets the minimum base pair length of ORFs to find. (Default: 93)')
    parser.add_argument('-p', '--preferred-ref', action='append',
                        help='Sets preferred reference(s) to annotate by. Input should be an NCBI accession number. '
                             'This can be used multiple times to construct a hierarchy of references to use. '
                             '(Default: None)')
    parser.add_argument('-e', '--e-value', type=float, default=1.0,
                        help='Sets the e-value to use for blastp. (Default: 1.0)')
    parser.add_argument('-w', '--word-size', type=int, default=2,
                        help='Sets the word size to use for blastp. (Default: 2)')
    parser.add_argument('-t', '--threads', type=int, default=1,
                        help='Sets the number of threads to use for blastp. (Default: 1)')
    parser.add_argument('-c', '--cut-off', type=float, default=0.0,
                        help='Sets a cut-off for choosing from possible annotations. This does two things: Firstly, '
                             'if B*cutoff>P with B being the best hit to an ORF and P being the best preferred '
                             'reference hit to that ORF, then the preferred reference is ignored. Secondly, the '
                             'cut-off is used for selecting a name for an ORF, all hits in the range B to B*cutoff are '
                             'considered and the most commonly used name from these is used if there is no preferred '
                             'reference or it has been ignored due to the previously mentioned condition. To disable '
                             'this feature, set the cut-off to 0.0. (Default: 0.0)')
    parser.add_argument('-2', '--tbl2asn-path', type=str, default=f'{os.path.dirname(os.path.realpath(__file__))}'
                                                                  f'/bin/tbl2asn_x64_25.8',
                        help='Sets the path to the tbl2asn binary to use for conversion. '
                             '(Default: <program path>/bin/tbl2asn_x64_25.8)')

    return parser


def run_orf_collection(path_to_viral_genome, path_to_metadata_db, tbl2asn_path, min_len=93, preferred_refs=None,
                       evalue=1.0, word_size=2, threads=1, cutoff=0.0):
    """
    Function that collects ORF positions in fasta files, blasts these against an automatically created protein database
    and exports the annotated longest possible ORFS to a GenBank file. Unmappable ORFs are added as a different track.

    :param path_to_viral_genome: str path to viral genome in fasta format.
    The same directory must contain a .sbt file with the same basename.
    :param min_len: int minimum length of open reading frames to consider
    :param path_to_metadata_db: str path to a genbank file containing the database to annotate from
    :param tbl2asn_path: str path to the tbl2asn binary
    :param preferred_refs: list of str names for references to use preferentially during annotation
    :param evalue: float e-value parameter to use for blastp
    :param word_size: int word_size parameter to use for blastp
    :param threads: int threads parameter to use for blastp
    :param cutoff: float indicating a threshold used for naming purposes
    :return: None
    """

    if not preferred_refs:
        preferred_refs = []

    genome = Genome(input_path=path_to_viral_genome)
    genome_orfs = OrfFinder(genome, min_len=min_len)
    genome_orfs.collect_orfs()
    genome_orfs.write_orfs_to_file()
    annotations = OrfAnnotator(orfs=genome_orfs, db_path=path_to_metadata_db, tbl2asn_path=tbl2asn_path,
                               preferred_refs=preferred_refs, evalue=evalue, word_size=word_size, threads=threads,
                               cutoff=cutoff)
    annotations.blastp_orfs()
    annotations.blastp_result_to_tbl()
    annotations.filtered_blastp_tbl_to_genbank()


class Genome:
    def __init__(self, input_path):
        self.input_path = os.path.abspath(input_path)  # must be .fsa and in same dir as .sbt for tbl2asn
        self.seqIO_record = SeqIO.read(self.input_path, "fasta")  # get nucleotide sequence
        self.sequence = str(self.seqIO_record.seq)
        self.name = self.seqIO_record.name
        self.description = self.seqIO_record.description
        self.seqIO_reverse_complement = \
        [rec.reverse_complement(id="rc_" + rec.id, description="reverse complement") for rec in
         SeqIO.parse(self.input_path, "fasta")][0]
        self.rc_sequence = str(self.seqIO_reverse_complement.seq)


class Orf:
    def __init__(self, start, stop, length, orf_id, sequence, strand):
        self.start = start
        self.stop = stop
        self.length = length
        self.orf_id = orf_id
        self.sequence = sequence
        self.strand = strand

    def __str__(self):
        return "id: {}, start: {}, stop: {}, length: {}, sequence: {}, strand: {}".format(
            self.orf_id, self.start, self.stop, self.length, self.sequence, self.strand)

    def __repr__(self):
        return self.__str__()


class OrfFinder:
    """
    Considers nucleotide sequence of single genome from FASTA file and collects ORF information.
    Finds longest possible ORFs.
    """

    def __init__(self, genome: Genome, start_codons=None, stop_codons=None, min_len=93,
                 output="output"):
        """
        Initializes object

        :param start_codons: List of strings
        :param stop_codons: List of strings
        :param min_len: integer of minimum ORF length
        """

        self.genome = genome
        if not start_codons:
            self.start_codons = ['ATG']
        else:
            self.start_codons = start_codons  # DNA!
        if not stop_codons:
            self.stop_codons = ["TAA", "TAG", "TGA"]
        else:
            self.stop_codons = stop_codons
        self.min_len = min_len
        self.orfs = []
        self.orf_id_helper = 1
        self.output_path = output
        self.orfs_file_path = self.output_path + "/" + self.genome.description + "_" + "ORFs.fna"
        self.orfs_AA_file_path = self.output_path + "/" + self.genome.description + "_" + "ORFs_AA.faa"

        os.makedirs(self.output_path, exist_ok=True)
        fasta_in_output_path = os.path.join(self.output_path, os.path.splitext(
            os.path.basename(self.genome.input_path))[0]+'.fsa')
        if not os.path.exists(fasta_in_output_path):
            os.symlink(self.genome.input_path, fasta_in_output_path)

    def __print__(self):
        return '{} ORFs in  {}'.format(len(self.orfs), self.genome.input_path)

    def collect_orfs(self):
        """
        Finds all stop codons and the respective furthest inframe start codon.
        They are added as a dictionaries to the object.

        :return: List of dictionaries holding ORF information. Position coordinates are zero-based.
        """

        stop_codon_positions = self.find_stop_codon_positions()
        print("Found ", len(stop_codon_positions), " stop codons.")
        self.find_longest_orfs(stop_codon_positions)
        print("Found ", len(self.orfs), " ORFs.")

        return self.orfs

    def find_stop_codon_positions(self):
        """
        Finds all stop codon positions in all frames on both strands.

        :return: List of dictionaries with positions and strand information for all stop codons
        """

        scanned_region = range(0, len(self.genome.sequence) - 2)
        forward_stop_codons = [{"pos": pos, "strand": "+"} for pos in scanned_region if
                               self.genome.sequence[pos:pos + 3]
                               in self.stop_codons]  # 0 based indices

        rc_stop_codons = [{"pos": pos, "strand": "-"} for pos in scanned_region if self.genome.rc_sequence[pos:pos + 3]
                          in self.stop_codons]  # 0 based indices

        all_stop_codons = forward_stop_codons + rc_stop_codons
        return all_stop_codons

    def find_longest_orfs(self, stop_codon_positions):
        """
        Processes stop codon information, in order to separate them by strand information.

        :param stop_codon_positions: List of dictionaries containing 1 based stop codon positions and strand information
        :return:
        """

        forward_stop_codon_positions = [scp["pos"] for scp in stop_codon_positions if scp["strand"] == "+"]
        self.get_orfs_from_stop_pos(self.genome.sequence, forward_stop_codon_positions)
        rc_stop_codon_positions = [scp["pos"] for scp in stop_codon_positions if scp["strand"] == "-"]
        self.get_orfs_from_stop_pos(self.genome.rc_sequence, rc_stop_codon_positions, strand="reverse_complement")

    def get_orfs_from_stop_pos(self, sequence, stop_codon_positions, strand=None):
        """
        For each given stop codon poistion, searchs for inframe start codon positions of the longest possible ORFs

        :param sequence: string of nucleotides
        :param stop_codon_positions: list of zero based index integers
        :param strand: empty = forward strand / "reverse_complement" = backward strand
        :return:
        """

        for stop_codon_pos_index in range(0, len(stop_codon_positions)):
            if stop_codon_positions[stop_codon_pos_index] >= self.min_len - 2:
                begin_for_start_codon_search = None
                start_codon_pos = None
                stop_codon_pos = stop_codon_positions[stop_codon_pos_index]  # is 0 based index
                if stop_codon_pos_index == 0:
                    begin_for_start_codon_search = 0 + stop_codon_positions[stop_codon_pos_index] % 3
                else:
                    for index in range(stop_codon_pos_index - 1, -1, -1):
                        # check if previously found stop codons are in frame
                        if OrfFinder.in_frame(stop_codon_positions[stop_codon_pos_index], stop_codon_positions[index]):
                            begin_for_start_codon_search = stop_codon_positions[index]
                            break
                        else:
                            begin_for_start_codon_search = 0 + stop_codon_positions[stop_codon_pos_index] % 3

                if isinstance(begin_for_start_codon_search, int) \
                        and (stop_codon_pos + 2 - begin_for_start_codon_search >= self.min_len):
                    start_codon_pos = self.find_furthest_start_codon(sequence=sequence,
                                                                     startpoint=begin_for_start_codon_search,
                                                                     stoppoint=stop_codon_pos)

                if start_codon_pos and start_codon_pos >= 5900 and stop_codon_pos <= 6012:
                    pass
                if start_codon_pos and stop_codon_pos + 2 - start_codon_pos + 1 >= self.min_len \
                        and OrfFinder.in_frame(start_codon_pos, stop_codon_pos):
                    self.add_orf_info_to_object(sequence=sequence, start_codon_pos=start_codon_pos,
                                                stop_codon_pos=stop_codon_pos, strand=strand)

    def find_furthest_start_codon(self, sequence, startpoint, stoppoint):
        """
        For a given stop codon position in a sequence, finds the furthest inframe start codon.

        :param sequence: string of nucleotides
        :param startpoint: integer index, zero based, builds the span in which to search for start codons, usually last
                           stop codon position
        :param stoppoint: integer index, zero based
        :return: integer index, zero based
        """

        scanned_region = range(startpoint, stoppoint, 3)  # zero based indices
        start_codon_positions_in_scanned_region = [pos for pos in scanned_region if sequence[pos:pos + 3]
                                                   in self.start_codons]  # return 0 based index of inframe start codons

        if start_codon_positions_in_scanned_region:
            return min(
                start_codon_positions_in_scanned_region)  # we want the longest possible ORF: return first start codon
        else:
            return None

    def add_orf_info_to_object(self, sequence, start_codon_pos, stop_codon_pos, strand=None):
        """
        Adds succesfully found ORF information as dictionaries to object.
        Recalculates postions for reverse strand to relate to forward strand.
        Indices are now 1-based after this step.

        :param sequence: string of nucelotides
        :param start_codon_pos: integer
        :param stop_codon_pos: integer
        :param strand: strand information
        :return:
        """

        assert start_codon_pos < stop_codon_pos
        if not strand:
            strand = "+"
        orf = sequence[int(start_codon_pos):stop_codon_pos + 3]
        orf_length = len(orf)
        if strand == "reverse_complement":
            strand = "-"
            start_codon_pos = len(self.genome.sequence) - (start_codon_pos + 1)  # zero based index
            stop_codon_pos = len(self.genome.sequence) - (
            stop_codon_pos + 3)  # because zero based index on reverse strand and codon is part of orf
        else:
            stop_codon_pos = stop_codon_pos + 2  # the whole stop codon is part of the ORF
        assert orf_length % 3 == 0

        if orf_length >= self.min_len:
            self.orfs.append(
                Orf(start=start_codon_pos + 1, stop=stop_codon_pos + 1, length=orf_length, orf_id=self.orf_id_helper,
                    sequence=orf, strand=strand)
            )
            self.orf_id_helper += 1

    def write_orfs_to_file(self):
        seqio_sequences = [SeqIO.SeqRecord(seq=Seq(orf.sequence, IUPAC.ambiguous_dna),
                                           id="ORF#" + str(orf.orf_id) + "_" + str(orf.start) + "_" + str(orf.stop)
                                              + "_" + orf.strand, description="")
                           for orf in self.orfs]

        # Write ORF information to FASTA file
        SeqIO.write(seqio_sequences, self.orfs_file_path, "fasta")
        print("Wrote \"" + self.orfs_file_path + "\" file containing ORF sequences. Position indices are zero based.")

        print("Translating ORFs to amino acid sequences.")
        prot_sequences = []

        for sequence in seqio_sequences:
            as_seq = sequence.seq.transcribe().translate(stop_symbol="")
            prot_sequences.append(
                SeqIO.SeqRecord(
                    seq=as_seq,
                    id=sequence.id + "_AA",
                    description=""

                ))
        SeqIO.write(prot_sequences, self.orfs_AA_file_path, "fasta")
        print("Wrote \"" + self.orfs_AA_file_path + "\" file containing translated ORF sequences.")

    @staticmethod
    def in_frame(pos: int, pos2: int):
        left = min(pos, pos2)
        right = max(pos, pos2)
        same_frame = (right - left) % 3 == 0
        other_codons_in_between = right >= left + 3

        return same_frame and other_codons_in_between


class OrfAnnotator:
    """
    Takes ORF information from OrfFinder object and collects annotations from local database.
    Can create GenBank file.
    """

    def __init__(self, orfs: OrfFinder, db_path, tbl2asn_path, preferred_refs=None, evalue=1.0, word_size=2, threads=1,
                 cutoff=0.0):
        self.orfs = orfs
        self.preferred_refs = preferred_refs
        self.db_path = db_path
        self.tbl2asn_path = tbl2asn_path
        self.evalue = evalue
        self.cleaned_up_blast = None
        self.blastp_raw_file_path = self.orfs.output_path + "/" + self.orfs.genome.description + "_blastp_result_raw.xml"
        self.blastp_filtered_file_path = self.orfs.output_path + "/" + str(os.path.basename(
            self.orfs.genome.input_path).split(".")[0]) + ".tbl"
        self.word_size = word_size
        self.threads = threads
        self.prot_db_title = None
        self.cutoff = cutoff
        self.validate()

    def validate(self):
        assert os.path.exists(self.db_path)

    def prepare_genbank_file_for_prot_db_creation(self):
        ignore_counter = 0
        with open(self.orfs.output_path + "/" + self.orfs.genome.name + "_translations.fa", 'w') as outfile:
            for seq in SeqIO.parse(self.db_path, 'genbank'):
                # gather infos
                accession_number = "{}.{}".format(seq.annotations['accessions'][0], seq.annotations['sequence_version'])
                name = seq.description
                for feature in seq.features:
                    if feature.type == 'CDS':
                        try:
                            translation = feature.qualifiers["translation"][0]
                        except KeyError:
                            translation = ""
                        try:
                            protein_id = feature.qualifiers["protein_id"][0]
                        except KeyError:
                            protein_id = "NA"
                        try:
                            feature_name = feature.qualifiers['standard_name'][0]
                        except KeyError:
                            try:
                                feature_name = feature.qualifiers['gene'][0]
                            except KeyError:
                                try:
                                    feature_name = feature.qualifiers['product'][0]
                                except KeyError:
                                    try:
                                        feature_name = feature.qualifiers['locus_tag'][0]
                                    except KeyError:
                                        feature_name = 'NA'

                        # write out stuff
                        if translation:
                            title = '|'.join(
                                [accession_number, name.replace(' ', '_'), protein_id, feature_name.replace(' ', '_')])
                            outfile.write('>{}\n{}\n'.format(title, translation))
                        else:
                            ignore_counter += 1
        print("Wrote ", self.orfs.output_path + "/" + self.orfs.genome.name + " translations. Ignored " + str(
            ignore_counter) + " sequences as no translation information was available.")

    def create_protein_db(self):
        self.prot_db_title = self.orfs.genome.name + "_protDB"

        self.prepare_genbank_file_for_prot_db_creation()
        make_blast_db_command = "makeblastdb -in {} -input_type fasta -dbtype prot -out {}".format(
            self.orfs.output_path + "/" + self.orfs.genome.name + "_translations.fa",
            self.orfs.output_path + "/" + self.prot_db_title)
        print("Creating blast DB with the following command\n:", make_blast_db_command)
        subprocess.call(make_blast_db_command, shell=True)

    def blastp_orfs(self, evalue=None):
        if evalue:
            self.evalue = evalue
        self.create_protein_db()
        blastx_cline = NcbiblastxCommandline(cmd="blastp", db=self.orfs.output_path + "/" + self.prot_db_title,
                                             query=self.orfs.orfs_AA_file_path,
                                             evalue=self.evalue, outfmt=5, out=self.blastp_raw_file_path,
                                             num_threads=self.threads, word_size=self.word_size)

        print("Running local protein blast with the following parameters:")
        print("\t", blastx_cline)
        blastx_cline()
        print("Wrote \"" + self.blastp_raw_file_path + "\" file containing raw blast results.")

    def find_preferred_refs_in_alignments(self, alignments):
        references = [alignment.hit_def.split("|")[0] for alignment in alignments]
        pref_ref_alignments = {}
        if self.preferred_refs:
            for preferred_reference in self.preferred_refs:
                for index, reference in enumerate(references):
                    if reference.startswith(preferred_reference):
                        pref_ref_alignments[preferred_reference] = alignments[index]
                        break
                else:
                    pref_ref_alignments[preferred_reference] = None

        return pref_ref_alignments

    def blastp_result_to_tbl(self):
        # read in blast results
        blast_records = NCBIXML.parse(open(self.blastp_raw_file_path))

        # go through all blast records and write out as necessary
        with open(self.blastp_filtered_file_path, 'w') as tbl:
            tbl.write('>Features\t{}\tfiltered_blast_results\n'.format(self.orfs.genome.description))
            for blast_record in blast_records:
                start_stop = blast_record.query.split("_")
                start = start_stop[1]
                stop = start_stop[2]
                if blast_record.alignments:  # blast record with alignments - so at least one fitting protein
                    # calculate similarity in percent for each alignment, also collect the highest sim per orf<->ref
                    # to clean up secondary alignments
                    product = 'NA'
                    protein_id = 'NA'
                    similarities = {}
                    for alignment in blast_record.alignments:
                        # sort hsps by evalue
                        if len(alignment.hsps) > 1:
                            alignment.hsps.sort(key=lambda x: x.expect)
                        alignment.similarity = alignment.hsps[0].identities / blast_record.query_length * 100
                        reference = alignment.hit_def.split('|')[0]
                        try:
                            if alignment.similarity > similarities[reference]:
                                similarities[reference] = alignment.similarity
                        except KeyError:
                            similarities[reference] = alignment.similarity

                    # clean alignments from secondary alignments, e.g. all alignments where there are better alignments
                    # for the respective orf - reference combination, for ties, keep every alignment in the tie
                    blast_record.alignments = [alignment for alignment in blast_record.alignments
                                               if alignment.similarity == similarities[alignment.hit_def.split('|')[0]]]

                    # sort alignments by similarity
                    blast_record.alignments.sort(key=lambda x: x.similarity, reverse=True)

                    # if we are using the cutoff thingy, first get all alignments to consider
                    if self.cutoff:
                        blast_record.alignments = [alignment for alignment in blast_record.alignments
                                                   if alignment.similarity >=
                                                   blast_record.alignments[0].similarity * self.cutoff]

                    # check if any pref refs in that range
                    pref_ref_alignments = self.find_preferred_refs_in_alignments(blast_record.alignments)

                    # if yes, use the first given (not best) where the gene exists for the name
                    for pref_ref in self.preferred_refs:
                        if pref_ref_alignments[pref_ref] and pref_ref_alignments[pref_ref].title.split("|")[-1] != 'NA':
                            product = pref_ref_alignments[pref_ref].title.split("|")[-1]
                            protein_id = pref_ref_alignments[pref_ref].title.split("|")[-2]
                            break

                    # if no or pref refs yield no product, create the consensus
                    if product == 'NA' and protein_id == 'NA':
                        # count all occuring names
                        count_dict = {}
                        best_alignment_dict = {}
                        for alignment in blast_record.alignments:
                            gene = alignment.title.split('|')[-1]
                            try:
                                count_dict[gene] += 1
                            except KeyError:
                                count_dict[gene] = 1
                                best_alignment_dict[gene] = alignment  # since the alignments are sorted, take first

                        # take the most often occuring name, prioritize non hypothetical, if same count take best
                        # alignment - if still the same, just take the "smaller" one ... just stay consistent
                        count_sorted = sorted([(count, gene) for gene, count in count_dict.items()])
                        nonhypo = [(count, gene) for count, gene in count_sorted if 'hypothetical' not in gene]
                        if nonhypo:  # if we have non hypothetical products just use these
                            product = best_alignment_dict[nonhypo[0][1]].title.split("|")[-1]
                            protein_id = best_alignment_dict[nonhypo[0][1]].title.split("|")[-2]
                        else:  # otherwise use first name
                            product = best_alignment_dict[count_sorted[0][1]].title.split("|")[-1]
                            protein_id = best_alignment_dict[count_sorted[0][1]].title.split("|")[-2]

                    # write out
                    # order: pref refs in input order, chosen ref if not in pref ref, rest sorted by sim
                    tbl.write('{}\t{}\tCDS\n'.format(start, stop))
                    tbl.write('\t\t\tproduct\t{}\n'.format(product))
                    tbl.write('\t\t\tprotein_id\t{}\n'.format(protein_id))
                    tbl.write('\t\t\tnote\t')
                    for pref_ref in self.preferred_refs:
                        if pref_ref_alignments[pref_ref]:
                            reference = pref_ref_alignments[pref_ref].hit_def.split('|')[0]
                            gene = pref_ref_alignments[pref_ref].title.split('|')[-1]
                            protein = pref_ref_alignments[pref_ref].title.split('|')[-2]
                            similarity = str(pref_ref_alignments[pref_ref].similarity)
                            similarity = similarity[0:similarity.index('.')+3]
                            tbl.write('Reference: {}, Gene: {}, Protein: {}, Similarity: {}; '.format(reference, gene,
                                                                                                      protein,
                                                                                                      similarity))
                        else:
                            tbl.write('Reference: {}, Gene: NA, Protein: NA, Similarity: NA; '.format(pref_ref))
                    for alignment in blast_record.alignments:
                        if alignment in pref_ref_alignments.values():
                            continue
                        reference = alignment.hit_def.split('|')[0]
                        gene = alignment.title.split('|')[-1]
                        protein = alignment.title.split('|')[-2]
                        similarity = str(alignment.similarity)
                        similarity = similarity[0:similarity.index('.') + 3]
                        tbl.write('Reference: {}, Gene: {}, Protein: {}, Similarity: {}; '.format(reference, gene,
                                                                                                  protein, similarity))
                    tbl.write('\n')

                else:  # blast record without alignment - put the orf into the unannotated track
                    # write out into misc_feature track
                    tbl.write('{}\t{}\tmisc_feature\n'.format(start, stop))
                    tbl.write("\t\t\tnote\t{}\n".format(blast_record.query[:-3]))  # omitting what?

    def filter_blastp_result(self):
        raw = open(self.blastp_raw_file_path)
        raw_records = NCBIXML.parse(raw)
        filtered_records = []
        evalues = None
        print("Applying filter on blast output...")
        if self.preferred_refs:
            assert type(self.preferred_refs == "list")
            print("... filtering preferred references (", self.preferred_refs, ") ...")
        # each record is one orf - target pairing with possibly several hsps
        for record in raw_records:
            smallest_evalue = None
            if record.alignments:
                for alignment in record.alignments:
                    if len(alignment.hsps) > 1:
                        # if there is more than one HSP, make sure they are sorted by e-value therefore later only the
                        # first HSP will be considered
                        alignment.hsps.sort(key=lambda x: x.expect)

                evalues = [alignment.hsps[0].expect for alignment in record.alignments]
            else:
                # no alignment information found. "Empty" record simply gets added
                filtered_records.append(record)
            if evalues:
                smallest_evalue = min(evalues)
            best_alignments = None
            if smallest_evalue:
                best_alignments = [alignment for alignment in record.alignments
                                   if alignment.hsps[0].expect <= smallest_evalue]
            if self.preferred_refs:
                smallest_evalue_ref = None
                for ref in reversed(self.preferred_refs):
                    evalues_ref = [alignment.hsps[0].expect for alignment in record.alignments
                                   if alignment.hit_def.startswith(ref)]
                    if evalues_ref:
                        smallest_evalue_ref = min(evalues_ref)
                    if smallest_evalue_ref:
                        best_alignments_ref = [alignment for alignment in record.alignments
                                               if alignment.hit_def.startswith(ref)
                                               and alignment.hsps[0].expect <= smallest_evalue_ref]
                        if best_alignments_ref and best_alignments:
                            best_alignments = best_alignments_ref + best_alignments
                            # else:
                            #     print("No blast result for reference ",ref, " for query ", record.query," found.")
            if best_alignments:
                filtered_record = deepcopy(record)  # will hold the fitlered info
                filtered_record.alignments = best_alignments
                filtered_records.append(filtered_record)

        print("...done")
        self.filtered_blastp_to_tbl(filtered_records)

    def filtered_blastp_to_tbl(self, records):
        if os.path.exists(self.blastp_filtered_file_path):
            print("Previous .tbl found! Deleting \"" + str(self.blastp_filtered_file_path) + "\"")
            os.remove(self.blastp_filtered_file_path)
        with open(self.blastp_filtered_file_path, "w") as tbl:
            tbl.write(">Features\t" + self.orfs.genome.description + "\tfiltered_blast_results" + "\n")
            for blast in records:
                start = blast.query.split("_")[1]
                stop = blast.query.split("_")[2]
                if blast.alignments:

                    alignments = []
                    for alignment in blast.alignments:
                        alignments.append({
                            "reference": alignment.hit_def.split("|")[0],
                            "gene": alignment.title.split("|")[-1],
                            "protein": alignment.title.split("|")[-2],
                            "similarity": round((alignment.hsps[0].identities / blast.query_length),
                                                3) * 100
                            # percent
                        })
                    references = [alignment.hit_def.split("|")[0] for alignment in blast.alignments]
                    if self.preferred_refs:
                        for preferred_reference in self.preferred_refs:
                            if not any([ref.startswith(preferred_reference) for ref in references]):
                                no_info_dict = {
                                    "reference": preferred_reference,
                                    "gene": "NA",
                                    "protein": "NA",
                                    "similarity": "NA"
                                }
                                alignments.append(no_info_dict)

                    # sort by similarity
                    alignments = sorted(alignments, key=self.similarity_sorting, reverse=True)
                    # put preferred refs to the front, if any
                    alignments = sorted(alignments, key=self.give_alignment_for_preferred_sorting)

                    tbl.write(start + "\t" + stop + "\tCDS" + "\n")
                    try:
                        best_product = [alignment for alignment in alignments if alignment["gene"] != "NA"][0]["gene"]
                    except IndexError:
                        best_product = alignments[0]["gene"]
                    try:
                        best_protein_id = \
                            [alignment for alignment in alignments if alignment["gene"] != "NA"][0]["protein"]
                    except IndexError:
                        best_protein_id = alignments[0]["protein"]
                    tbl.write("\t\t\tproduct\t" + best_product + "\n")
                    tbl.write("\t\t\tprotein_id\t" + best_protein_id + "\n")
                    note_string = "\t\t\tnote\t"

                    for alignment_dict in alignments:
                        note_string += ("Reference:" + alignment_dict["reference"])
                        note_string += (", Gene:" + alignment_dict["gene"])
                        note_string += (", Protein:" + alignment_dict["protein"])
                        note_string += (", Similarity:" + str(alignment_dict["similarity"]) + "; ")

                    tbl.write(note_string + "\n")
                else:
                    tbl.write(start + "\t" + stop + "\tmisc_feature" + "\n")
                    # not found ORFs will be put on the misc_feature track only for distinction purposes
                    tbl.write("\t\t\tnote\t" + blast.query[:-3] + "\n")

        print("Wrote \"" + self.blastp_filtered_file_path + "\" file containing filtered blast results.")

    def give_alignment_for_preferred_sorting(self, alignment):
        for index, pref_ref in enumerate(self.preferred_refs):
            if alignment["reference"] in pref_ref:
                return index
        return len(self.preferred_refs)

    @staticmethod
    def similarity_sorting(alignment):
        try:
            return float(alignment["similarity"])
        except ValueError:  # in case we have a preferred ref without alignment, we get NA here
            return 0.0

    def filtered_blastp_tbl_to_genbank(self):
        dir_of_script = os.path.dirname(os.path.realpath(__file__))
        print("Starting GenBank file generation.")
        subdirs = [x[0] for x in os.walk(dir_of_script)]
        print("\tSearching for .tbl and .sbt template file...", end="")

        template_files = []
        template = None
        tbl_files = []
        for subdir in subdirs:
            for file in os.listdir(subdir):
                if file.endswith(".sbt"):
                    template_files.append(subdir + "/" + file)
                elif file.endswith(".tbl"):
                    tbl_files.append(file)

        if template_files:
            if len(template_files) > 1:
                print("\n\tMultiple template files found in this directory:")
                for temp in range(0, len(template_files)):
                    print("\t", temp, ": \t", template_files[temp])
                print("Using the first one (default choice).")
                choice = 0
                template = template_files[int(choice)]
                print("\tUsing 0:", template)
            else:
                template = template_files[0]
                print("\tfound. \n\tUsing", template)
        else:
            print("\tNo template file provided. You can create one under this URL: ")
            print("\t\t https://submit.ncbi.nlm.nih.gov/genbank/template/submission/")
            print(
                "\tSave it in this directory or a subdirectory and rerun this step of the routine, by calling the \"" +
                sys._getframe().f_code.co_name + "\" method!"
                )
        if not tbl_files:
            print("\tno .tbl file was found. Remember creating it with the respective method: filter_blastp_result()")
        elif template:
            print(".tbl file found.")
            tbl2asn_call = " ".join(
                [self.tbl2asn_path, "-p", "./"+self.orfs.output_path, "-t", str(template), "-V b -r output"])
            print("Generating GenBank file using tbl2asn:")
            print("\t", tbl2asn_call)
            subprocess.call(tbl2asn_call, shell=True)

        gbf_there = False
        sqn_there = False
        subdirs = [x[0] for x in os.walk(dir_of_script)]
        for subdir in subdirs:
            for file in os.listdir(subdir):
                if file.endswith(".gbf"):
                    gbf_there = True
                elif file.endswith(".sqn"):
                    sqn_there = True

        if gbf_there and sqn_there:
            print("GenBank files successfully created.")
        else:
            print("GenBank files were not created.")


def main():
    parser = setup_cli_parser()
    cli_args = parser.parse_args(sys.argv[1:])
    run_orf_collection(path_to_viral_genome=cli_args.input, path_to_metadata_db=cli_args.database,
                       tbl2asn_path=cli_args.tbl2asn_path, min_len=cli_args.minimum_length,
                       preferred_refs=cli_args.preferred_ref, evalue=cli_args.e_value, word_size=cli_args.word_size,
                       threads=cli_args.threads, cutoff=cli_args.cut_off)


if __name__ == '__main__':
    main()
